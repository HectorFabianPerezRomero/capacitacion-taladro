﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDrill
{
    private AudioClip clickSound;
    private AudioSource audioSource;
    private GameObject onButton;
    private GameObject offButton;
    private bool animateOn = false;
    private bool animateOff = false;
    private int buttonVelocity = 2;

    public ButtonDrill(GameObject onButton, GameObject offButton, AudioClip clip)
    {
        this.onButton = onButton;
        this.offButton = offButton;
        clickSound = clip;
        audioSource = this.onButton.GetComponent<AudioSource>();
    }

    /**
     * Función que verifica si se realiza clic sobre el objeto del que se esta agregando
     * este script
     */
    public string checkClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rayCastCollision();
        }

        animateOn = AnimateButton(animateOn, onButton);
        animateOff = AnimateButton(animateOff, offButton);

        if (animateOn) return "on";
        if (animateOff) return "off";

        return "standby";
    }
    private void rayCastCollision()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            /*if (hit.transform)
                Debug.Log(hit.transform.name);*/
            if (hit.transform.name.Equals(onButton.transform.name))
            {
                animateOn = true;
                audioSource.PlayOneShot(clickSound);
            }
            if (hit.transform.name.Equals(offButton.transform.name))
            {
                animateOff = true;
                audioSource.PlayOneShot(clickSound);
            }
        }
    }

    private bool AnimateButton(bool animate, GameObject button)
    {

        if (animate)
        {
            /*Debug.Log("Entra al primer if");
            Debug.Log(button.transform.localPosition.x);*/
            button.transform.Translate(new Vector3(Time.deltaTime/10 , 0, 0));
        }


        if (button.transform.localPosition.x >= 20.27f)
        {
            /*Debug.Log("Entra al segundo if");
            Debug.Log(button.transform.localPosition.x);*/
            animate = false;
        }

        if (!animate && (button.transform.localPosition.x > 20.03f))
        {
            /*Debug.Log("Entra al tercer if");
            Debug.Log(button.transform.localPosition.x);*/
            button.transform.Translate(new Vector3(-(Time.deltaTime/10 ), 0, 0));
        }

        return animate;
    }
}
