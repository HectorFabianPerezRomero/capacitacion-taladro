﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimation : MonoBehaviour
{

    /* Public attributes */
    public AudioClip clickSound;
    public AudioSource audioSource;
    public GameObject button;
    public int buttonVelocity = 2;

    /* Private attributes */
    private bool animate = false;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            rayCastCollision();
            animateButton();
        }

        if (animate)
        {
            button.transform.Translate(new Vector3(Time.deltaTime * buttonVelocity, 0, 0));
        }


        if (transform.localPosition.x >= 20.27)
        {
            animate = false;
        }

        if (!animate && transform.localPosition.x > 20.03)
        {
            button.transform.Translate(new Vector3(-(Time.deltaTime * buttonVelocity), 0, 0));
        }
    }

    private void animateButton()
    {
        animate = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
    }

    private void rayCastCollision()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            if (hit.transform)
                Debug.Log(hit.transform.name);
            if (hit.transform.name == button.transform.name)
                this.audioSource.PlayOneShot(this.clickSound);
        }

    }
}
