﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

public class TaladroAnimacion : MonoBehaviour
{
    /* Public attributes */
    public Transform lever;
    public GameObject baseMaterial;
    public GameObject mandril;
    public GameObject palancaBase;
    public GameObject onButton;
    public GameObject offButton;
    public GameObject bit;
    public int velocity = 10;
    public AudioClip clickSound;
    public AudioClip drillSound;
    public float leverDegree = 0, leverDegreeTemp;

    /* Private attributes */
    private float yRotationLever = 0f;
    private float yActualDegree = 0f;
    private string buttonState = "standby";
    private string serialMessage;
    private bool drillState = false, serialAvailable = true, start = false, sendSecondLimit = false;
    private AudioSource drillSounds;
    private DrillCollider drillCollider;
    private SerialPort sp;
    private float degrees = 0f;
    private int contador = 0, bloqueo = 0, bloqueoTemp = 0, bloqueoArduino = 0;

    /* Clases personalizadas */
    private ButtonDrill buttons;
    /*private DrillCollider bitColliders;*/

    void Start()
    {
        // Se instancian las clases que manejan los botones
        buttons = new ButtonDrill(onButton, offButton, clickSound);
        drillSounds = GetComponent<AudioSource>();

        /*Debug.Log(mandril.transform.localPosition);*/

        drillCollider = bit.GetComponent<DrillCollider>();

        try
        {
            sp = new SerialPort("COM7", 2000000);
            sp.Open();
            sp.ReadTimeout = 1;

            initSerialCom();
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }

        drillCollider.sp = sp;
    }

    // Update is called once per frame
    void Update()
    {

        // Se revisa el puerto serial si hay información entrante
        checkSerialPort(sp);

        if (start)
        {
            checkLever();
            // Verifica clic en el botón de encendido o apagado
            buttonState = buttons.checkClick();
            // verifica el estado del taladro
            drillState = checkdrillState(buttonState, drillState);
            // Verifica el estado del taladro y su sonido
            drillSetstate(drillState, mandril);
            // Mueve la posición vertical del mandril de a cuerdo a los grados de la palanca
            moveMandrill(yActualDegree, mandril);
            // En dado caso de que no se esté activando la palanca, se realiza el retorno
            /*yActualDegree = checkReturnMandril(mandril, yActualDegree);*/
            // Se revisa si hay alguna colisión de la broca
            checkDrillCollision(drillCollider);
        }
        else if(contador == 60)
        {
            contador = 0;
            initSerialCom();
        }
        else
            contador++;

        /*Debug.Log(drillCollider.collisionBit);*/
    }

    /*
     * 
     */
    private void checkDrillCollision(DrillCollider drillCollider)
    {
        if (drillCollider.collisionMaterial || drillCollider.collisionBit)
        {
            if (serialAvailable && leverDegree != bloqueoArduino)
            {
                serialAvailable = false;
                bloqueo = (int)Math.Round(Math.Abs(yActualDegree));
                bloqueoTemp = (int)Math.Round(Math.Abs(yActualDegree)) - 4;
                serialMessage = "blo" + bloqueo;
                /*sendSecondLimit = true;*/
                Debug.Log("Enviando bloqueo: " + serialMessage.ToString());
                sendData(serialMessage.ToString());
            }
        }
        else
        {
            /*sendData("fre");*/
        }
    }

    /**
     * 
     * 
     */
    private void checkSerialPort(SerialPort sp)
    {
        if (sp.IsOpen)
        {
            try
            {
                /*Debug.Log("Leyendo puerto serial...");*/
                string message = sp.ReadLine();
                if (float.TryParse(message,out leverDegreeTemp))
                {
                    leverDegree = leverDegreeTemp;
                    Debug.Log("grados palanca: " + leverDegree);
                }
                else
                {
                    Debug.Log(message);
                    if (message.Equals("fin")) serialAvailable = true;
                    if (message.Equals("enabled")) {
                        start = true;
                    }
                    if (message.Substring(0, 3).Equals("blo"))
                    {
                        if (int.TryParse(message.Substring(3), out bloqueoArduino))
                        {
                            Debug.Log("bloqueo arduino: " + bloqueoArduino);
                        }
                    }
                }
                /*Debug.Log("grados recibidos:");*/
            }
            catch (System.Exception e)
            {
                /*Debug.Log(e.Message);*/
            }
        }
    }

    private void initSerialCom() {
        if (sp.IsOpen)
        {
            try
            {
                Debug.Log("Enviando inicio...");
                sp.WriteLine("ini");
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    private void sendData(String message)
    {
        if (sp.IsOpen)
        {
            try
            {
                sp.WriteLine(message);
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    /**
     * Función que verifica si se está activando la palabca y en caso contrario
     * se realiza el retorno de la palanca a su estado original o reposo.
     * 
     * @param mandril {GameObject} Objeto del mandril
     * @param yActualDegree {float} Estado actual en grados de la palanca
     * 
     * @return yActualDegree {float} El nuevo estado en grados de la palanca
     * */
    private float checkReturnMandril(GameObject mandril, float yActualDegree)
    {
        if (!Input.GetKey(KeyCode.S))
        {
            if (yActualDegree < 0 && yActualDegree >= -90)
            {
                yRotationLever = Time.deltaTime * velocity;
                if (yActualDegree <= 0)
                {
                    yActualDegree += yRotationLever;
                    lever.Rotate(new Vector3(0, yRotationLever, 0));
                }
            }
        }

        return yActualDegree;
    }

    /**
     * Función para realizar el movimiento del mandril de acuerdo a los grados
     * que tiene la palanca de movimiento.
     * 
     * @param yActualDegree {float} Grados actuales de la palanca
     * @param madril {GameObject} Objeto del mandrill en la aplicación
     */
    private void moveMandrill(float yActualDegree, GameObject mandril)
    {
        float y = ((-0.1022f) * (-yActualDegree)) + 52.7f;

        Vector3 mandrilPos = new Vector3(mandril.transform.localPosition.x, mandril.transform.localPosition.y, y);

        /*mandril.transform.Translate(new Vector3(0, 0, -Time.deltaTime));*/
        mandril.transform.localPosition = mandrilPos;
    }

    /**
     * Función para revisar el estado actual del taladro, si este se encuentra encendido
     * o apagado y realizar la ejecución de los sonidos
     */
    private void drillSetstate(bool drillState, GameObject mandril)
    {
        if (drillState)
        {
            if (!drillSounds.isPlaying) drillSounds.Play();
            mandril.transform.Rotate(new Vector3(0, 0, Time.deltaTime * 2000));
        }
        else drillSounds.Stop();
    }

    /**
     * Función para realizar el movimiento de la palanca de acuerdo a
     * las teclas W (Subir) y S (Bajar)
     */
    private void checkLever()
    {
        // Método con las teclas W y S
        /*if (Input.GetKey(KeyCode.W))
        {
            yRotationLever = Time.deltaTime * velocity;
            if (yActualDegree <= 0)
            {
                yActualDegree += yRotationLever;
                lever.Rotate(new Vector3(0, yRotationLever, 0));
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            yRotationLever = -(Time.deltaTime * velocity);
            if (yActualDegree >= -90)
            {
                yActualDegree += yRotationLever;
                lever.Rotate(new Vector3(0, yRotationLever, 0));
            }
        }*/

        // Método con grados
        float diffDegrees = 0;
        diffDegrees = leverDegree + yActualDegree;

        /*if (yActualDegree != leverDegree)*/
        if (diffDegrees != 0)
        {
            if (diffDegrees > 0)
            {
                /*yRotationLever = -(Time.deltaTime * velocity);*/
                yRotationLever = -diffDegrees;
                if (yActualDegree >= -120)
                {
                    yActualDegree += yRotationLever;
                    lever.Rotate(new Vector3(0, yRotationLever, 0));
                }
            }
            else if (diffDegrees < 0)
            {
                /*yRotationLever = Time.deltaTime * velocity;*/
                yRotationLever = -diffDegrees;
                if (yActualDegree <= 0)
                {
                    yActualDegree += yRotationLever;
                    lever.Rotate(new Vector3(0, yRotationLever, 0));
                }
            }
        }

    }

    /**
     * Función para ervisar el estado del taladro, si esta en funcionamiento
     * o no.
     */
    private bool checkdrillState(string buttonState, bool drillstate)
    {
        switch (buttonState.ToString())
        {
            case "on":
                if (!drillstate)
                {
                    bit.gameObject.tag = "DrillOn";
                    return true;
                }
                break;
            case "off":
                if (drillstate) {
                    bit.gameObject.tag = "DrillOff";
                    return false;
                }
                break;
            case "standby":
                return drillstate;
        }
        return false;
    }
}