﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Material : MonoBehaviour
{
    // Start is called before the first frame update

    public float resistencia = 2;

    private bool drilling = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (drilling)
        {
            Debug.Log("Resistencia: " + resistencia);
            resistencia -= Time.deltaTime;
        }

        if (resistencia <= 0) Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Tabla:Colision con" + other.tag);
        if (other.gameObject.tag.Equals("DrillOn")) drilling = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("DrillOn")) drilling = false;
    }


}
