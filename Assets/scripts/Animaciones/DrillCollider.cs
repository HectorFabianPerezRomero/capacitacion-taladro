﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

public class DrillCollider : MonoBehaviour
{
    public bool collisionBit = false;
    public bool collisionMaterial = false;
    public float resistencia = 10;
    public SerialPort sp;
    public bool serialAvailable;
    
    Collider otro;
    private bool revisarCollider = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*if (collisionMaterial) resistencia -= Time.deltaTime;*/
        if(!otro && revisarCollider)
        {
            revisarCollider = false;
            collisionMaterial = false;
            collisionBit = false;
            if (sp.IsOpen)
            {
                try
                {
                    Debug.Log("Liberando palanca, se elimino tabla");
                    sp.WriteLine("fre");

                }
                catch (System.Exception e)
                {
                    Debug.Log(e.Message);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Drill:Colision con" + other.tag);
        otro = other;
        revisarCollider = true;
        if (other.gameObject.tag.Equals("MaterialDrill"))
            collisionMaterial = true;
        if (other.gameObject.tag.Equals("DrillBase"))
            collisionBit = true;
        /*Debug.Log("Se ha colisionado el objeto con: ");
        Debug.Log();*/
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("MaterialDrill"))
            collisionMaterial = false;
        if (other.gameObject.tag.Equals("DrillBase"))
            collisionBit = false;
        otro = other;

        if (sp.IsOpen)
        {
            try
            {
                Debug.Log("Liberando palanca");
                sp.WriteLine("fre");
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}
