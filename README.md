# Capacitación taladro

Repositorio para el proyecto de la escena en 3D de la materia Sistemas de inmersión de la Universidad Central, Semestre 2019-II.

## Prerequisitos

- Unity, Descargar de la página oficial [aquí](https://store.unity.com/es). Opción para [Unity Dark Skin](https://gbatemp.net/threads/tutorial-dark-theme-in-unity-personal-edition-2019.547731/)
- Debe tener instalado un programa de gestión de versionamiento (GIT), puede encontrarlo para Windows [aquí](https://git-scm.com/download/win), o para [Linux](https://git-scm.com/download/linux) o para [MAC](https://git-scm.com/download/mac). Instale el programa con las opciones por defecto que aparecen.

## Instalación

Para usar obtener el código de este proyecto debe realizar los siguientes pasos:

1. En la parte superior del repositorio aparecerá un botón que dice "clone", saldra una ventana con un recuadro para copiar un comando que comienza con: git clone... Copie todo el comando que aparece ahí.

2. Luego de copiar la URL diríjase a la consola de windows (CMD -> por búsqueda rápida de windows) o la terminal en Linux. Luego de ya estar en la consola:
   ```
   cd /ruta/a/la/carpeta/donde/guardar/el/proyecto
   ```
   Puede ayudarse con la tecla TAB para que la consola le vaya dando sugerencias o doble TAB para todas las sugerencias.

3. Ya al tener la carpeta donde quiere guardar el proyecto, ingrese el comando copiado del apartado 1 y luego ENTER. De forma automática se creará una carpeta con el nombre del proyecto, la consola le dirá el proceso y cuando haya terminado

4. Puede abrir el proyecto con Unity.

## Para tener en cuenta

- Por el momento podrá hacer cambios de forma local, no podrá subir cambios al repositorio.
- Este sistema no trabaja como una Nube, por lo que no se verán reflejados los cambios de manera automática.

## Traer nuevos cambios del repositorio

Si no ha realizado cambios en archivos de Scripts o de Assets, puede traer cambios nuevos del repositorio de la siguiente forma:

1. Diríjase en la consola o terminal hasta la carpeta del proyecto donde se creó el proyecto en la instalación
   ```
   cd /ruta/hasta/el/proyecto/capacitacion-taladro
   ```
   **Debe estar _DENTRO_ de la carpeta raíz del proyecto**, no sobre la carpeta inmediatamente anterior.

2. Digitar el siguiente comando:
   ```
   git pull
   ```
   En caso de que le arroje error, puede intentar con:
   ```
   git pull origin master
   ```

3. Actualizar o esperar que Unity realice la actualización general y ya podrá revisar cambios.